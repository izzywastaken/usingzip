﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZipArchive
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Using Archive");
            UsingArchive();
            Console.ReadLine();
        }

        private static void UsingArchive()
        {
            var zipFile = @"D:\myArchive.zip";
            var filesToArchive = Directory.GetFiles(@"D:\HTML-CSS");

            using (var archive = ZipFile.Open(zipFile, ZipArchiveMode.Create))
            {
                foreach (var filePath in filesToArchive)
                {
                    archive.CreateEntryFromFile(filePath, Path.GetFileName(filePath));
                }
            }
            Console.WriteLine("Archive was created");
        }
    }
}
